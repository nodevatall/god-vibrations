﻿using UnityEngine;
using System.Collections;


// ---------------------------------------------------
///<summary>
/// Class
///</summary>
public class BubbleShield : MonoBehaviour
{

    #region Definition
    #endregion

    #region Attributes
    private PlayerLife _owner;
    private Collider2D _collider;
    #endregion

    // ---------------------------------------------------
    ///<summary>
    /// Start
    ///</summary>
    void Start()
    {
        this.Init();
    }

    // ---------------------------------------------------
    ///<summary>
    /// Start
    ///</summary>
    void Init()
    {
        this._owner = this.transform.parent.GetComponent<PlayerLife>();
        this._collider = this.GetComponent<Collider2D>();
    }

    // ---------------------------------------------------
    ///<summary>
    /// OnTriggerEnter2D
    ///</summary>
    void OnTriggerEnter2D(Collider2D obj)
    {
        //Check if owner defined
        if (!this._owner) this.Init();

        //Check if Player
        if (obj.gameObject.tag != "Player") return;

        //Ignore Owner
        PlayerLife player = obj.GetComponent<PlayerLife>();
        PlayerControllerMartin controller = obj.GetComponent<PlayerControllerMartin>();
        if (player == this._owner) return;
        if (!controller) return;

        //Collision point
        //Vector3 direction = obj.transform.position - this.transform.position;
		controller.FxBubble();
        controller.meFaireRepulser(this.transform.position);

    }
}