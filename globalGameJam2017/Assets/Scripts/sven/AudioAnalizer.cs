﻿using UnityEngine;
using System.Collections;


// ---------------------------------------------------
///<summary>
/// AudioAnalizer
///</summary>
public class AudioAnalizer : MonoBehaviour
{

    #region Definition
    public enum Channel
    {
        LEFT = 0,
        RIGHT = 1,
        BOTH = 2
    }
    #endregion

    #region Attributes
    public AudioLevel audioLevel;

    [SerializeField]
    private string microphone;
    public bool mix;
    public bool playOnStart = false;
    public FFTWindow analyzeMode = FFTWindow.BlackmanHarris;
    public Channel channel;

    public Bumpers bumpers;


    private AudioSource _audio;
    private AudioSource _mic;
    private int _bmp = 140;

    public float[] _mult;
    public float[] _microphoneMult;

    private float[] _samples;
    private int currentBeat = 0;
    private int nbBumpers = 0;
    private int nbAverage = 1;
    private int averageOffset = 0;
    private float minBumperY = -8.6f;

    private bool corouteRunning = false;

    private bool _analyzeSong = false;
    private bool _analyzeMic = false;
    private int _sampling = 64;
    private float _maxAverage = 0;

    #endregion

    // ---------------------------------------------------
    ///<summary>
    /// Start
    ///</summary>
    void Start()
    {
        //Sampling
        this._samples = new float[this._sampling];

        //Get Audio Sources
        this._audio = this.GetComponent<AudioSource>();
        this._mic = this.gameObject.AddComponent<AudioSource>();

        //Audio Params
        this._audio.mute = false;
        this._audio.loop = true;

        //AudioLevel params
        if (this.audioLevel)
        {
            this._audio.clip = this.audioLevel.clip;
            this._mult = this.audioLevel.mult;
        }

        //Microphone params
        if (variablesGlobales.microphoneActiver && Microphone.devices.Length > 0)
        {
            this.microphone = Microphone.devices[0];
            this.mix = true;
        }
        if (!string.IsNullOrEmpty(this.microphone))
        {
            SetMicrophoneDevice(this.microphone);
        }

        //Average
        this.nbBumpers = this.bumpers.list.Count;
        this.nbAverage = Mathf.FloorToInt(this._samples.Length / this.nbBumpers);
        this.averageOffset = Mathf.FloorToInt((this._samples.Length % this.nbBumpers) / 2);

        //Play On Start
        if (this.playOnStart) this.Play();
    }

    // --------------------------------------------------
    /// <summary>
    /// SetMicrophoneDevice
    /// Call before Play()
    /// </summary>
    /// <param name="device">Device name (see Microphone.devices)</param>
    /// <returns>If device was found and setted</returns>
    public bool SetMicrophoneDevice(string device)
    {
        //Check device
        bool deviceFound = false;
        for(int d=0; d<Microphone.devices.Length; d++)
        {
            if (Microphone.devices[d] == device) deviceFound = true;
        }
        if (!deviceFound) return false;

        //Init microphone
        this.microphone = device;
        int min;
        int max;
        Microphone.GetDeviceCaps(this.microphone, out min, out max);
        this._mic.mute = false;
        this._mic.loop = true;
        this._mic.volume = 0.01f;
        this._mic.pitch = 0.95f;
        this._mic.clip = Microphone.Start(this.microphone, true, 1, min);

        //Confirm
        return true;
    }

    // ---------------------------------------------------
    ///<summary>
    /// Play
    ///</summary>
    public void Play()
    {
        //Start Audio
        this._analyzeSong = false;
        this._analyzeMic = false;
        if (this._audio.clip != null) this._audio.Play();
        if (this._mic.clip != null)
        {
            this._mic.Play();
            this._analyzeMic = true;
            if (this._audio.clip != null && this.mix) this._analyzeSong = true;
        }
        else this._analyzeSong = true;

        //Starting Wave
        int min = 0;
        int max = 9;
        int ran = Random.Range(min, max);
        if(ran % 2 > 0) this.StartCoroutine(this._LeftToRight());
        else this.StartCoroutine(this._RightToLeft());
    }

    // ---------------------------------------------------
    ///<summary>
    /// SetAudio
    ///</summary>
    public void Play(AudioClip clip, int bmp, float[] mult)
    {
        //Set Params
        this._audio.clip = clip;
        this._bmp = bmp;
        this._mult = mult;

        //Start Audio
        this._analyzeSong = false;
        this._analyzeMic = false;
        this._audio.Play();
        if (this._mic.clip != null)
        {
            this._mic.Play();
            this._analyzeMic = true;
            if (this._audio.clip != null && this.mix) this._analyzeSong = true;
        }
        else this._analyzeSong = true;

        //Starting Wave
        int min = 0;
        int max = 9;
        int ran = Random.Range(min, max);
        if (ran % 2 > 0) this.StartCoroutine(this._LeftToRight());
        else this.StartCoroutine(this._RightToLeft());
    }

    // ---------------------------------------------------
    ///<summary>
    /// Stop
    ///</summary>
    public void Stop()
	{
		//Start Audio
		this._audio.Stop();
	}

    // ---------------------------------------------------
    ///<summary>
    /// Update
    ///</summary>
    void FixedUpdate()
    {
        //Check if audio playing
        if ((!this._audio || !this._audio.isPlaying) && (!this._mic || !this._mic.isPlaying)) return;

        //Check if corouteRunning
        if (this.corouteRunning) return;

        //Fill Samples
        this._GetSamples();

        float[] mult = this._mult;
        if (!this._analyzeSong && this._analyzeMic) mult = this._microphoneMult;
            
        //Pulse Bumpers
        for (int i=0; i<this.nbBumpers; i++)
        {
            //Check Bumper
            GameObject bumper = this.bumpers.list[i];
            if (!bumper.activeInHierarchy || bumper.transform.position.y > minBumperY)
            {
                continue;
            }

            //Calcul Average
            float force = 0;
            for(int m=0; m< this.nbAverage; m++)
            {
                force += this._samples[Mathf.Min(i + m + this.averageOffset, this._samples.Length-1)];
            }
            force /= this.nbAverage;
            force *= mult[i] * 3 * 3500;
			if(variablesGlobales.modeBardBall)
			{
				if(force > 6000) force = 6000;
			}
			else if(variablesGlobales.numStage == 6)
			{
				if(force > 5000) force = 5000;
			}
			else if(force > 7000) force = 7000;

            //Apply
            this.bumpers.list[i].GetComponent<Rigidbody2D>().AddForce(Vector3.up * force);
        }
    }

    // ---------------------------------------------------
    /// <summary>
    /// Launch a wave from left to right
    /// </summary>
    /// <returns></returns>
    private void _GetSamples()
    {
        //Clear samples float
        this._samples = new float[this._sampling];

        //Analyze Song
        if (this._analyzeSong)
        {
            float[] songSamples = new float[this._sampling];
            if (this.channel == Channel.BOTH)
            {
                float[] left = new float[this._sampling];
                this._audio.GetSpectrumData(left, 0, this.analyzeMode);
                float[] right = new float[this._sampling];
                this._audio.GetSpectrumData(right, 1, this.analyzeMode);
                for (int i = 0; i < this._sampling; i++)
                {
                    songSamples[i] = Mathf.Max(left[i], right[i]);
                }
            }
            else
            {
                this._audio.GetSpectrumData(songSamples, (int)this.channel, this.analyzeMode);
            }
            this._samples = songSamples;
        }

        //Analyze Mic
        if (this._analyzeMic)
        {
            float[] micSamples = new float[this._sampling];
            if (this.channel == Channel.BOTH)
            {
                float[] left = new float[this._sampling];
                this._mic.GetSpectrumData(left, 0, this.analyzeMode);
                float[] right = new float[this._sampling];
                this._mic.GetSpectrumData(right, 1, this.analyzeMode);
                for (int i = 0; i < this._sampling; i++)
                {
                    micSamples[i] = Mathf.Max(left[i] * 500, right[i] * 500);
                }
            }
            else
            {
                this._mic.GetSpectrumData(micSamples, (int)this.channel, this.analyzeMode);
            }
            if (!this._analyzeSong) this._samples = micSamples;
            else {
                for (int i = 0; i < this._sampling/2; i++)
                {
                    this._samples[i] = Mathf.Max(this._samples[i], micSamples[i] * 500);
                    this._samples[i+1] = Mathf.Max(this._samples[i+1], micSamples[i] * 500);
                }
            }
        }
    }

    // ---------------------------------------------------
    /// <summary>
    /// Launch a wave from left to right
    /// </summary>
    /// <returns></returns>
    private IEnumerator _LeftToRight()
    {
        this.corouteRunning = true;
        yield return new WaitForSeconds(0.2f);
        int force = 4500;
        for (int i = 0; i < this.nbBumpers; i++)
        {
            if (!this.bumpers.list[i].activeInHierarchy) continue;
            this.bumpers.list[i].GetComponent<Rigidbody2D>().AddForce(Vector3.up * force);
            yield return new WaitForSeconds(0.02f);
        }
        this.corouteRunning = false;
    }

    // ---------------------------------------------------
    /// <summary>
    /// Launch a wave from right to left
    /// </summary>
    /// <returns></returns>
    private IEnumerator _RightToLeft()
    {
        this.corouteRunning = true;
        yield return new WaitForSeconds(0.2f);
        int force = 4500;
        for (int i = this.nbBumpers-1; i > -1; i--)
        {
            if (!this.bumpers.list[i].activeInHierarchy) continue;
            this.bumpers.list[i].GetComponent<Rigidbody2D>().AddForce(Vector3.up * force);
            yield return new WaitForSeconds(0.02f);
        }
        this.corouteRunning = false;
    }

    /**************************************************************************
    *   Static
    *
    *
    *
    ***************************************************************************/
    public static float[] GetDefaultMult()
    {
        float[] defaultMult = new float[16]
        {
            4,
            8,
            15,
            20,
            20,
            25,
            25,
            25,
            30,
            30,
            30,
            35,
            35,
            35,
            40,
            40
        };
        return defaultMult;
    }
}