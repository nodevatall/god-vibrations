﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public GameObject ParentJoueur;
	public GameObject God1;
	public GameObject God2;
	public GameObject God3;
	public GameObject God4;
	public GameObject LabelInfo1;
	public GameObject LabelInfo2;
	public GameObject LabelTemps;
	public GameObject LabelMusique;
	public AudioClip FxStart;
	public AudioClip FxResultat;
	public AudioClip FxItemActivated;
	public AudioClip FxExtraTime;
	public AudioClip MusiqueExtraTime;
	public AudioSource AudioS;
	public GameObject PanelResultat;
	public GameObject PanelPause;

	//Voix
	public AudioClip FxItemShield;
	public AudioClip FxItemInversion;
	public AudioClip FxItemBubble;
	public AudioClip FxItemFreeze;
	public AudioClip FxPublicJakuta;
	public AudioClip FxPublicAnubis;
	public AudioClip FxPublicTyr;
	public AudioClip FxPublicHuitzi;
	public AudioClip Fx10s;
	public AudioClip Fx30s;

	private GameObject P1;
	private GameObject P2;
	private GameObject P3;
	private GameObject P4;
	private int nbrJoueurs;
	private int tempsMatch;
	private bool prolongation;
	private int nbrJoueurValidatePausePlay;
	private int nbrJoueurValidatePauseQuit;

	// Use this for initialization
	void Start ()
	{
		variablesGlobales.etatJeu = "car";
		PanelResultat.SetActive(false);
		LabelMusique.SetActive(false);
		LabelTemps.GetComponent<Text> ().text = "";
		tempsMatch = 120;
		prolongation = false;
		nbrJoueurs = variablesGlobales.nbrPlayers;
		//Vie
		variablesGlobales.vieP1 = 3;
		variablesGlobales.vieP2 = 3;
		if(variablesGlobales.nbrPlayers > 2) variablesGlobales.vieP3 = 3;
		else variablesGlobales.vieP3 = 0;
		if(variablesGlobales.nbrPlayers > 3) variablesGlobales.vieP4 = 3;
		else variablesGlobales.vieP4 = 0;
		//Les joueurs
		//P1
		if (variablesGlobales.warriorP1 == 1)
			P1 = Instantiate (God1, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP1 == 2)
			P1 = Instantiate (God2, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP1 == 3)
			P1 = Instantiate (God3, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP1 == 4)
			P1 = Instantiate (God4, ParentJoueur.transform) as GameObject;
		P1.transform.GetComponent<PlayerControllerMartin> ().SetIdJoueur (1);
		//Vie
		P1.transform.FindChild("Life").FindChild("Vie1").GetComponent<SpriteRenderer>().color = Color.red;
		P1.transform.FindChild("Life").FindChild("Vie1 (1)").GetComponent<SpriteRenderer>().color = Color.red;
		P1.transform.FindChild("Life").FindChild("Vie1 (2)").GetComponent<SpriteRenderer>().color = Color.red;
		//Volume audio
		P1.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		//P2
		if (variablesGlobales.warriorP2 == 1)
			P2 = Instantiate (God1, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP2 == 2)
				P2 = Instantiate (God2, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP2 == 3)
				P2 = Instantiate (God3, ParentJoueur.transform) as GameObject;
		else if (variablesGlobales.warriorP2 == 4)
				P2 = Instantiate (God4, ParentJoueur.transform) as GameObject;
		P2.transform.GetComponent<PlayerControllerMartin> ().SetIdJoueur (2);
		//Vie
		P2.transform.FindChild("Life").FindChild("Vie1").GetComponent<SpriteRenderer>().color = Color.blue;
		P2.transform.FindChild("Life").FindChild("Vie1 (1)").GetComponent<SpriteRenderer>().color = Color.blue;
		P2.transform.FindChild("Life").FindChild("Vie1 (2)").GetComponent<SpriteRenderer>().color = Color.blue;
		//Volume audio
		P2.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		//P3
		if (variablesGlobales.nbrPlayers > 2) {
			if (variablesGlobales.warriorP3 == 1)
						P3 = Instantiate (God1, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP3 == 2)
						P3 = Instantiate (God2, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP3 == 3)
						P3 = Instantiate (God3, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP3 == 4)
						P3 = Instantiate (God4, ParentJoueur.transform) as GameObject;
			P3.transform.GetComponent<PlayerControllerMartin> ().SetIdJoueur (3);
			//Vie
			P3.transform.FindChild("Life").FindChild("Vie1").GetComponent<SpriteRenderer>().color = new Color(0,0.8f,0.2f);
			P3.transform.FindChild("Life").FindChild("Vie1 (1)").GetComponent<SpriteRenderer>().color = new Color(0,0.8f,0.2f);
			P3.transform.FindChild("Life").FindChild("Vie1 (2)").GetComponent<SpriteRenderer>().color = new Color(0,0.8f,0.2f);
			//Volume audio
			P3.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		}
		//P4
		if (variablesGlobales.nbrPlayers > 3) {
			if (variablesGlobales.warriorP4 == 1)
						P4 = Instantiate (God1, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP4 == 2)
						P4 = Instantiate (God2, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP4 == 3)
						P4 = Instantiate (God3, ParentJoueur.transform) as GameObject;
			else if (variablesGlobales.warriorP4 == 4)
						P4 = Instantiate (God4, ParentJoueur.transform) as GameObject;
			P4.transform.GetComponent<PlayerControllerMartin> ().SetIdJoueur (4);
			//Vie
			P4.transform.FindChild("Life").FindChild("Vie1").GetComponent<SpriteRenderer>().color = new Color(0.8f,0,0.9f);
			P4.transform.FindChild("Life").FindChild("Vie1 (1)").GetComponent<SpriteRenderer>().color = new Color(0.8f,0,0.9f);
			P4.transform.FindChild("Life").FindChild("Vie1 (2)").GetComponent<SpriteRenderer>().color = new Color(0.8f,0,0.9f);
			//Volume audio
			P4.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		}
		//Volume audio
		AudioS.GetComponent<AudioSource>().volume = variablesGlobales.volumeFx;
		//Start
		StartCoroutine(GestionDepart());
	}

	IEnumerator GestionDepart() {
		LabelInfo1.GetComponent<Text> ().text = "Ready ?";
		//Fx
		AudioS.GetComponent<AudioSource>().PlayOneShot(FxStart);
		yield return new WaitForSeconds(1.2f);
		variablesGlobales.etatJeu = "play";
		LabelInfo1.GetComponent<Text> ().text = "3";
		yield return new WaitForSeconds(1);
		LabelInfo1.GetComponent<Text> ().text = "2";
		yield return new WaitForSeconds(0.7f);
		LabelInfo1.GetComponent<Text> ().text = "1";
		yield return new WaitForSeconds(1f);
		LabelInfo1.GetComponent<Text> ().text = "";
		//Chrono
		StartCoroutine(Chrono());
		//Creation des pieges
		GameObject.Find("World").GetComponent<GestionPiege>().StartCreatePique();
		//Si micro
		if(variablesGlobales.microphoneActiver)
		{
			GameObject.Find("AudioAnalizer").GetComponent<AudioAnalizer>().Play();
			GameObject.Find("AudioAnalizer").GetComponent<AudioAnalizer>().Stop();
		}
		else GameObject.Find("AudioAnalizer").GetComponent<AudioAnalizer>().Play();
		//Label musique en cours
		StartCoroutine(AfficherMusique());
	}

	IEnumerator AfficherMusique()
	{
		string nomMusique = "";
		LabelMusique.SetActive(true);
		LabelMusique.transform.FindChild("Label").GetComponent<Text>().text = variablesGlobales.tabMusics[variablesGlobales.numStage - 1]; 
		yield return new WaitForSeconds(3f);
		LabelMusique.SetActive(false);
	}

	//Chrono
	IEnumerator Chrono()
	{
		string tempsAafficher = "";
		int moduloSecondes = tempsMatch%60;
		int minutes = (tempsMatch - moduloSecondes)/60;
		if(minutes > 0) {
			if(moduloSecondes < 10) tempsAafficher = minutes.ToString() + ":0" + moduloSecondes.ToString();
			else tempsAafficher = minutes.ToString() + ":" + moduloSecondes.ToString();
		}
		else {
			if(moduloSecondes < 10) tempsAafficher = "0" + moduloSecondes.ToString();
			else tempsAafficher = moduloSecondes.ToString();
		}
		LabelTemps.GetComponent<Text> ().text = tempsAafficher;
		//S'il reste 10s
		if(tempsMatch == 10) AudioS.GetComponent<AudioSource>().PlayOneShot(Fx10s);
		//S'il reste 30s
		else if(tempsMatch == 30) AudioS.GetComponent<AudioSource>().PlayOneShot(Fx30s);
		yield return new WaitForSeconds(1);
		tempsMatch--;
		//Gestion prolongation
		if(!prolongation && tempsMatch == 0 && variablesGlobales.etatJeu == "play")
		{
			prolongation = true;
			//Fx
			GameObject.Find("AudioAnalizer").GetComponent<AudioAnalizer>().Stop();
			AudioS.GetComponent<AudioSource>().PlayOneShot(FxExtraTime);
			LabelTemps.GetComponent<Text> ().color = Color.red;
			LabelTemps.GetComponent<Text> ().text = "Extra Time";
			//Musique de l'extra time
			//Si micro
			if(variablesGlobales.microphoneActiver == false)
			{
				GameObject.Find("AudioAnalizer").GetComponent<AudioAnalizer>().audioLevel = Resources.Load<AudioLevel>("AudioLevels/ExtraTime");
				GameObject.Find("AudioAnalizer").GetComponent<AudioSource>().clip = MusiqueExtraTime;
				GameObject.Find("AudioAnalizer").GetComponent<AudioAnalizer>().Play();
			}
			yield return new WaitForSeconds (1);
			tempsMatch = 20;
			moduloSecondes = tempsMatch%60;
			minutes = (tempsMatch - moduloSecondes)/60;
			if(minutes > 0) {
				if(moduloSecondes < 10) tempsAafficher = minutes.ToString() + ":0" + moduloSecondes.ToString();
				else tempsAafficher = minutes.ToString() + ":" + moduloSecondes.ToString();
			}
			else {
				if(moduloSecondes < 10) tempsAafficher = "0" + moduloSecondes.ToString();
				else tempsAafficher = moduloSecondes.ToString();
			}
			LabelTemps.GetComponent<Text> ().text = tempsAafficher;
			//Trous acceleres
			GameObject.Find("World").GetComponent<GestionPiege>().createTrouExtraTime();
		}
		//Egalite
		else if(prolongation && tempsMatch == 0 && variablesGlobales.etatJeu == "play") 
		{
			variablesGlobales.etatJeu = "bilanDraw";
			LabelInfo1.GetComponent<Text>().text = "Draw!";
			moduloSecondes = tempsMatch%60;
			minutes = (tempsMatch - moduloSecondes)/60;
			if(minutes > 0) {
				if(moduloSecondes < 10) tempsAafficher = minutes.ToString() + ":0" + moduloSecondes.ToString();
				else tempsAafficher = minutes.ToString() + ":" + moduloSecondes.ToString();
			}
			else {
				if(moduloSecondes < 10) tempsAafficher = "0" + moduloSecondes.ToString();
				else tempsAafficher = moduloSecondes.ToString();
			}
			LabelTemps.GetComponent<Text>().text = tempsAafficher;
			GameObject.Find("Pieges").SetActive(false);
			//Fx
			GameObject.Find("AudioAnalizer").GetComponent<AudioAnalizer>().Stop();
			AudioS.GetComponent<AudioSource>().PlayOneShot (FxResultat);
			Invoke ("LancerPanneauResultatDraw", 2);
		}
		//Suite chrono
		if(variablesGlobales.etatJeu == "play")
		{
			StartCoroutine(Chrono());
		}
	}

	void Update()
	{
		if(variablesGlobales.etatJeu == "bilan")
		{
			if(Input.GetButtonUp("Submit") || Input.GetButtonUp("Submit2") || Input.GetButtonUp("Submit3") || Input.GetButtonUp("Submit4") || Input.GetButtonUp("Submit5") || Input.GetButtonUp("Submit6") || Input.GetButtonUp("Submit7") || Input.GetButtonUp("Submit8"))
			{
				variablesGlobales.etatJeu = "replay";
				GestionSuitePanneauResultat(true);
			}
			else if(Input.GetButtonUp("Cancel") || Input.GetButtonUp("Cancel2") || Input.GetButtonUp("Cancel3") || Input.GetButtonUp("Cancel4") || Input.GetButtonUp("Cancel5") || Input.GetButtonUp("Cancel6") || Input.GetButtonUp("Cancel7") || Input.GetButtonUp("Cancel8"))
			{
				variablesGlobales.etatJeu = "quit";
				GestionSuitePanneauResultat(false);
			}
		}
		else if(variablesGlobales.etatJeu == "play")
		{
			//Gestion pause
			if(((Input.GetButtonUp("Submit") || Input.GetButtonUp("Submit5")) && (Input.GetButtonUp("Cancel") || Input.GetButtonUp("Cancel5"))) ||
			   ((Input.GetButtonUp("Submit2") || Input.GetButtonUp("Submit6")) && (Input.GetButtonUp("Cancel2") || Input.GetButtonUp("Cancel6"))) ||
				((Input.GetButtonUp("Submit3") || Input.GetButtonUp("Submit7")) && (Input.GetButtonUp("Cancel3") || Input.GetButtonUp("Cancel7"))) ||
				((Input.GetButtonUp("Submit4") || Input.GetButtonUp("Submit8")) && (Input.GetButtonUp("Cancel4") || Input.GetButtonUp("Cancel8"))))
			{
				variablesGlobales.etatJeu = "pause";
				Time.timeScale = 0;
				GameObject.Find("AudioAnalizer").GetComponent<AudioSource>().Pause();
				PanelPause.SetActive(true);
				nbrJoueurValidatePausePlay = 0;
				nbrJoueurValidatePauseQuit = 0;
				PanelPause.transform.FindChild("Panneau").FindChild("Consigne1").FindChild("Label").GetComponent<Text>().text = "Play " + nbrJoueurValidatePausePlay + "/" + variablesGlobales.nbrPlayers;
				PanelPause.transform.FindChild("Panneau").FindChild("Consigne2").FindChild("Label").GetComponent<Text>().text = "Quit " + nbrJoueurValidatePauseQuit + "/" + variablesGlobales.nbrPlayers;
			}
		}
		else if(variablesGlobales.etatJeu == "pause")
		{
			if(Input.GetButtonUp("Submit") || Input.GetButtonUp("Submit2") || Input.GetButtonUp("Submit3") || Input.GetButtonUp("Submit4") || Input.GetButtonUp("Submit5") || Input.GetButtonUp("Submit6") || Input.GetButtonUp("Submit7") || Input.GetButtonUp("Submit8"))
			{
				nbrJoueurValidatePausePlay++;
				PanelPause.transform.FindChild("Panneau").FindChild("Consigne1").FindChild("Label").GetComponent<Text>().text = "Play " + nbrJoueurValidatePausePlay + "/" + variablesGlobales.nbrPlayers;
			}
			else if(Input.GetButtonUp("Cancel") || Input.GetButtonUp("Cancel2") || Input.GetButtonUp("Cancel3") || Input.GetButtonUp("Cancel4") || Input.GetButtonUp("Cancel5") || Input.GetButtonUp("Cancel6") || Input.GetButtonUp("Cancel7") || Input.GetButtonUp("Cancel8"))
			{
				nbrJoueurValidatePauseQuit++;
				PanelPause.transform.FindChild("Panneau").FindChild("Consigne2").FindChild("Label").GetComponent<Text>().text = "Quit " + nbrJoueurValidatePauseQuit + "/" + variablesGlobales.nbrPlayers;
			}
			//On reprend la partie
			if(nbrJoueurValidatePausePlay == variablesGlobales.nbrPlayers)
			{
				GameObject.Find("AudioAnalizer").GetComponent<AudioSource>().Play();
				PanelPause.SetActive(false);
				Time.timeScale = 1;
				variablesGlobales.etatJeu = "play";
			}
			//On quitte la partie
			else if(nbrJoueurValidatePauseQuit == variablesGlobales.nbrPlayers)
			{
				Time.timeScale = 1;
				variablesGlobales.etatJeu = "quit";
				GestionSuitePanneauResultat(false);
			}
		}
	}

	//Quand un joueur meurt
	public void GestionControleFin(int idP)
	{
		if(variablesGlobales.etatJeu == "play")
		{
			if (idP == 1) {
				P1.SetActive (false);
				variablesGlobales.vieP1 = 0;
			} else if (idP == 2) {
				P2.SetActive (false);
				variablesGlobales.vieP2 = 0;
			} else if (idP == 3) {
				P3.SetActive (false);
				variablesGlobales.vieP3 = 0;
			} else if (idP == 4) {
				P4.SetActive (false);
				variablesGlobales.vieP4 = 0;
			}
			nbrJoueurs--;
			//Fin du jeu
			if (nbrJoueurs == 1) {
				variablesGlobales.etatJeu = "bilanMort";
				LabelInfo1.GetComponent<Text> ().text = "Victory!";
				GameObject.Find("Pieges").SetActive(false);
				//Fx
				GameObject.Find("AudioAnalizer").GetComponent<AudioAnalizer>().Stop();
				AudioS.GetComponent<AudioSource>().PlayOneShot(FxResultat);
				Invoke("LancerPanneauResultat", 2);
			}
			StartCoroutine (FondAnim ());
		}
	}

	public void RandomCriDuPublic(int numJoueur)
	{
		//Potentiel soutient du public
		int soutientPublic = (int)Random.Range(1, 3.5f);
		print("soutientPublic = " + soutientPublic);
		if(soutientPublic == 2)
		{
			if(numJoueur == 1) StartCoroutine(CriDuPublic(variablesGlobales.warriorP1));
			else if(numJoueur == 2) StartCoroutine(CriDuPublic(variablesGlobales.warriorP2));
			else if(numJoueur == 3) StartCoroutine(CriDuPublic(variablesGlobales.warriorP3));
			else if(numJoueur == 4) StartCoroutine(CriDuPublic(variablesGlobales.warriorP4));
		}
	}

	//Public en folie
	private IEnumerator CriDuPublic(int numDieu)
	{
		//Attente commentaire
		float attenteCom = (int)Random.Range(1, 4.5f);
		yield return new WaitForSeconds(attenteCom);
		if(variablesGlobales.etatJeu == "play")
		{
			if(numDieu == 1) AudioS.GetComponent<AudioSource>().PlayOneShot(FxPublicAnubis);
			else if(numDieu == 2) AudioS.GetComponent<AudioSource>().PlayOneShot(FxPublicHuitzi);
			else if(numDieu == 3) AudioS.GetComponent<AudioSource>().PlayOneShot(FxPublicJakuta);
			else if(numDieu == 4) AudioS.GetComponent<AudioSource>().PlayOneShot(FxPublicTyr);
		}
	}

	//Anim fond mort
	IEnumerator FondAnim()
	{
		//Debut tremblement fond
		GameObject.Find("Walls").transform.FindChild("fond").GetComponent<Animator>().speed = 80;
		yield return new WaitForSeconds(1f);
		GameObject.Find("Walls").transform.FindChild("fond").GetComponent<Animator>().speed = 1;
	}

	//Afficher panneau resultat
	private void LancerPanneauResultat()
	{
		variablesGlobales.etatJeu = "bilan";
		LabelInfo1.GetComponent<Text> ().text = "";
		PanelResultat.SetActive(true);
		//EventSystem.current.SetSelectedGameObject(PanelResultat.gameObject);
		//Vainqueur
		List<int> ListePerdants = new List<int>();
		int vainqueur = 1;
		int vieTemp = variablesGlobales.vieP1;
		if(variablesGlobales.vieP2 > vieTemp)
		{
			vieTemp = variablesGlobales.vieP2;
			ListePerdants.Add(vainqueur);
			vainqueur = 2;
		}
		else ListePerdants.Add(2);
		if(variablesGlobales.vieP3 > vieTemp)
		{
			vieTemp = variablesGlobales.vieP3;
			ListePerdants.Add(vainqueur);
			vainqueur = 3;
		}
		else ListePerdants.Add(3);
		if(variablesGlobales.vieP4 > vieTemp)
		{
			vieTemp = variablesGlobales.vieP4;
			ListePerdants.Add(vainqueur);
			vainqueur = 4;
		}
		else ListePerdants.Add(4);
		PanelResultat.transform.FindChild ("Titre").GetComponent<Text> ().text = "PLAYER " + vainqueur + " WINS!";
		//Score
		if(vainqueur == 1) variablesGlobales.scoreP1++;
		else if(vainqueur == 2) variablesGlobales.scoreP2++;
		else if(vainqueur == 3) variablesGlobales.scoreP3++;
		else if(vainqueur == 4) variablesGlobales.scoreP4++;
		//Skin du vainqueur et des perdants
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP1").FindChild("Joueur1").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP1) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP2").FindChild("Joueur2").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP2) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP3").FindChild("Joueur3").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP3) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP4").FindChild("Joueur4").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP4) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP1").FindChild("Joueur1").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP2").FindChild("Joueur2").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP3").FindChild("Joueur3").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP4").FindChild("Joueur4").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		if(vainqueur == 1) PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP1").FindChild("Joueur1").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 1f);
		else if(vainqueur == 2) PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP2").FindChild("Joueur2").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 1f);
		else if(vainqueur == 3) PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP3").FindChild("Joueur3").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 1f);
		else if(vainqueur == 4) PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP4").FindChild("Joueur4").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 1f);
		//Scores totaux
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP1").GetComponent<Text> ().text = "P1\n" + variablesGlobales.scoreP1;
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP2").GetComponent<Text> ().text = "P2\n" + variablesGlobales.scoreP2;
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP3").GetComponent<Text> ().text = "P3\n" + variablesGlobales.scoreP3;
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP4").GetComponent<Text> ().text = "P4\n" + variablesGlobales.scoreP4;
		//Nombre de joueurs
		if (variablesGlobales.nbrPlayers < 3)
		{
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP3").gameObject.SetActive(false);
		}
		else {
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP3").gameObject.SetActive(true);
		}
		if (variablesGlobales.nbrPlayers < 4)
		{
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP4").gameObject.SetActive(false);
		}
		else {
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP4").gameObject.SetActive(true);
		}
	}

	private void LancerPanneauResultatDraw()
	{
		variablesGlobales.etatJeu = "bilan";
		LabelInfo1.GetComponent<Text> ().text = "";
		PanelResultat.SetActive(true);
		PanelResultat.transform.FindChild ("Titre").GetComponent<Text> ().text = "DRAW!";
		//Skin des joueurs
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP1").FindChild("Joueur1").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP1) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP2").FindChild("Joueur2").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP2) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP3").FindChild("Joueur3").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP3) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP4").FindChild("Joueur4").FindChild("Skin").GetComponent<Image>().sprite = Resources.Load<Sprite>("SkinsWarriors/warrior" + variablesGlobales.warriorP4) as Sprite;
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP1").FindChild("Joueur1").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP2").FindChild("Joueur2").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP3").FindChild("Joueur3").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		PanelResultat.transform.FindChild("Scores").FindChild ("ScoreP4").FindChild("Joueur4").FindChild("Skin").GetComponent<Image>().color = new Color(255,255,255, 0.3f);
		//Scores totaux
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP1").GetComponent<Text> ().text = "P1\n" + variablesGlobales.scoreP1;
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP2").GetComponent<Text> ().text = "P2\n" + variablesGlobales.scoreP2;
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP3").GetComponent<Text> ().text = "P3\n" + variablesGlobales.scoreP3;
		PanelResultat.transform.FindChild("Scores").FindChild("ScoreP4").GetComponent<Text> ().text = "P4\n" + variablesGlobales.scoreP4;
		//Nombre de joueurs
		if (variablesGlobales.nbrPlayers < 3)
		{
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP3").gameObject.SetActive(false);
		}
		else {
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP3").gameObject.SetActive(true);
		}
		if (variablesGlobales.nbrPlayers < 4)
		{
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP4").gameObject.SetActive(false);
		}
		else {
			PanelResultat.transform.FindChild("Scores").FindChild("ScoreP4").gameObject.SetActive(true);
		}
	}

	public void GestionSuitePanneauResultat(bool continuer)
	{
		if(continuer)
		{
			print ("Continuer !");
			string nameScene = "";
			variablesGlobales.numStage = (int)Random.Range(1, 6.5f);
			switch(variablesGlobales.numStage)
			{
				case 1:
					nameScene = "SceneEgypt";
					break;
				case 2:
					nameScene = "SceneAztec";
					break;
				case 3:
					nameScene = "SceneNordic";
					break;
				case 4:
					nameScene = "SceneArfica";
					break;
				case 5:
					nameScene = "SceneBarde";
					break;
				case 6:
					nameScene = "SceneChickenRune";
					break;
			}
			SceneManager.LoadScene(nameScene);
		}
		else if(!continuer)
		{
			print ("Quitter !");
			SceneManager.LoadScene("SceneMenuV2");
		}
	}

	//Gestion des feedbacks des items
	public void FeedbackItems(Item.ItemType idItem)
	{
		switch(idItem)
		{
			case Item.ItemType.BUBBLE:
				LabelInfo2.GetComponent<Text>().text = "Bubble!";
				//Commentaire
				AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemBubble);
				break;
			case Item.ItemType.INVINCIBILITY:
				LabelInfo2.GetComponent<Text>().text = "Shield!";
				//Commentaire
				AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemShield);
				break;
			case Item.ItemType.INVERSION:
				LabelInfo2.GetComponent<Text>().text = "Inversion!";
				//Commentaire
				AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemInversion);
				break;
			case Item.ItemType.FREEZE:
				LabelInfo2.GetComponent<Text>().text = "Freeze!";
				//Commentaire
				AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemFreeze);
				break;
			case Item.ItemType.LIFE:
				LabelInfo2.GetComponent<Text>().text = "Life!";
				break;
		}
		//Fx item active
		AudioS.GetComponent<AudioSource>().PlayOneShot(FxItemActivated);
		Invoke("FinFeedbackItems", 1.5f);
	}
	private void FinFeedbackItems()
	{
		LabelInfo2.GetComponent<Text>().text = "";
	}
}
