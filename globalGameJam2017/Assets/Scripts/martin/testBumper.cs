﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class testBumper : MonoBehaviour {
    public float forceBumpMin=250f;
    public float forceBumpMax = 500f;
    public List<GameObject> lesBumpers;
	

    void Start()
    {
        InvokeRepeating("lancerVague", 5.0f, 2.0f);
    }
    private void lancerVague()
    {
        foreach(GameObject unBumper in lesBumpers)
        {
            unBumper.GetComponent<Rigidbody2D>().AddForce(transform.up* Random.Range(forceBumpMin, forceBumpMax));
        }
        //print("BumperLancer");
    }
    public void lancerVagueWithIntensity(int idBumber,float intensiter)
    {
        forceBumpMax = intensiter;
        
        lesBumpers[idBumber].GetComponent<Rigidbody2D>().AddForce(transform.up * forceBumpMax);
        
        //print("BumperLancer");
    }
}
