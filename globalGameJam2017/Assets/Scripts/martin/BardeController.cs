﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BardeController : MonoBehaviour {
    private bool _enMouvement = false;
    public float vitesseDeplacement = 5f;
    public GameObject amphore;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (_enMouvement)
        {
            this.transform.position = Vector2.Lerp(this.GetComponent<Transform>().position, new Vector3(0,0,0), (Time.fixedDeltaTime * vitesseDeplacement));
            if(this.transform.position.x>=-1  && this.transform.position.y == 0)
            {
                _enMouvement = false;
                //print("finish");
                lancerAmphore();
            }
        }
	}
    public void startDeplacement()
    {
        //print("Mouvement lancer");
        _enMouvement = true;
    }
    public void lancerAmphore()
    {
        GameObject monAmphore = Instantiate(amphore, GameObject.Find("Pieges").transform);
        monAmphore.transform.position = new Vector3(0, 0, 0);
        monAmphore.GetComponent<AmphoreController>().startDeplacement(new Vector3(4,0,0));

    }
}
