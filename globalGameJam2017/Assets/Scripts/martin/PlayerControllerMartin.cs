﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerMartin : MonoBehaviour
{
    public AudioClip audioHit;
    public AudioClip audioDash;
    public AudioClip audioDeath;
	public AudioClip audioCollision;
	public AudioClip audioBubble;
    public int idJoueur = -1;
    private string _nomControllerHorizontal = "Horizontal";
    private string _nomControllerVertical = "Vertical";
    private string _nomControllerSubmit = "Submit";
    private bool _canDash = true;
    private bool _dashing = false;
    private bool _repulsion = false;
    //Other :
    private float m_MaxSpeed = 5f;
    private float m_JumpForce = 20f;
    private bool m_AirControl = true;
    [SerializeField] private LayerMask m_WhatIsGround;
    private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    private bool m_Grounded;            // Whether or not the player is grounded.
    private Transform m_CeilingCheck;   // A position marking where to check for ceilings
    const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
    private Animator m_Anim;            // Reference to the player's animator component.
    private Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = false;  // For determining which way the player is currently facing.
    public bool reverse;

    void Awake()//Valider !
    {
        //print(variablesGlobales.lesControllersActifs.Count);
        /*foreach(PlayerControllerIdentifier unController in variablesGlobales.lesControllersActifs)
        {
            if (unController.idSlotPlayer == idJoueur)
            {
                if (unController.id > 1)
                {
                    _nomControllerHorizontal += unController.id;
                    _nomControllerVertical += unController.id;
                    _nomControllerSubmit += unController.id;
                    //print(_nomControllerHorizontal);
                    //print(_nomControllerVertical);
                    //print(_nomControllerSubmit);
                }
            }
        }*/
		
        // Setting up references.
        m_GroundCheck = transform.Find("GroundCheck");
        m_CeilingCheck = transform.Find("CeilingCheck");
        m_Anim = GetComponent<Animator>();
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.name.Contains("Player"))
        {
			//Fx
			if(variablesGlobales.etatJeu == "play" || variablesGlobales.etatJeu == "playBardeball") this.transform.GetComponent<AudioSource>().PlayOneShot(audioCollision);
			if (coll.gameObject.GetComponent<PlayerControllerMartin>() != null)
            {
                coll.gameObject.GetComponent<PlayerControllerMartin>().meFaireRepulser(new Vector2(this.transform.position.x,this.transform.position.y));
            }
        }
            

    }

	public void SetIdJoueur(int id) {
		idJoueur = id;
        foreach (PlayerControllerIdentifier unController in variablesGlobales.lesControllersActifs)
        {
            if (unController.idSlotPlayer == idJoueur)
            {
                if (unController.id > 1)
                {
                    _nomControllerHorizontal += unController.id;
                    _nomControllerVertical += unController.id;
                    _nomControllerSubmit += unController.id;
                    //print(_nomControllerHorizontal);
                    //print(_nomControllerVertical);
                    //print(_nomControllerSubmit);
                }
            }
        }
    }

    private void FixedUpdate()//Valider !
    {
        //print(Input.GetAxis(_nomControllerHorizontal));
        if (Input.GetAxis(_nomControllerSubmit) == 1 && _canDash && !_repulsion && !_dashing)
        {
            _canDash = false;
            _dashing = true;
            setAnimDash();
        }
        if (!_dashing && !_repulsion)
        {
            Move(Input.GetAxis(_nomControllerHorizontal));
            m_Grounded = false;
            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    m_Grounded = true;
                    if (!_canDash)
                    {
                        //print("Recharge DASH");
                        _canDash = true;
                    }

                }

            }
            m_Anim.SetBool("Ground", m_Grounded);
            // Set the vertical animation
            m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);
        }
        
        
    }
    
    public void Move(float move)
    {
        //only control the player if grounded or airControl is turned on
        if (m_Grounded || m_AirControl)
        {
            //Reverse
            if (this.reverse) move *= -1;

            // The Speed animator parameter is set to the absolute value of the horizontal input.
            m_Anim.SetFloat("Speed", Mathf.Abs(move));

            // Move the character
            m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.y);

            // If the input is moving the player right and the player is facing left...
            if (move > 0 && !m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }
        }
    }
    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    public void setAnimHit()
    {
        this.transform.GetComponent<AudioSource>().PlayOneShot(audioHit);
        m_Anim.SetTrigger("HitTrigger");
    }
    public void setAnimDash()
    {
        this.transform.GetComponent<AudioSource>().PlayOneShot(audioDash);
        m_Anim.SetTrigger("DashTrigger");
        int dir = 0;
        if (Input.GetAxis(_nomControllerHorizontal) > 0) dir = 1;
        else if (Input.GetAxis(_nomControllerHorizontal) < 0) dir = -1;
        if (dir != 0)
        {
            if (this.reverse) dir *= -1;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(dir * 15, 0);
            //this.GetComponent<Rigidbody2D>().AddForce(-transform.right * 100, ForceMode2D.Force);
        }
    }
    public void setAnimDeath()
    {
        this.transform.GetComponent<AudioSource>().PlayOneShot(audioDeath);
        m_Anim.SetTrigger("DeathTrigger");
    }
	public void setEndDeath(){
		GameObject.Find ("World").GetComponent<GameManager>().GestionControleFin (gameObject.GetComponent<PlayerControllerMartin>().idJoueur);
	}
    public void finishDashing()
    {
        _dashing = false;
    }
    public void meFaireRepulser(Vector2 positionEnnemi)
    {
        _repulsion = true;
        Vector2 direction = new Vector2(this.transform.position.x - positionEnnemi.x, this.transform.position.y - positionEnnemi.y);
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(direction.x * 15, direction.y * 15);
        Invoke("stopRepulsion", 0.3f);
    }
    private void stopRepulsion()
    {
        _repulsion = false;
    }

	public void FxBubble()
	{
		this.transform.GetComponent<AudioSource>().PlayOneShot(audioBubble);
	}
}
