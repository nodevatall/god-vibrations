﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionBall : MonoBehaviour {

	//Variable
	public GameObject GM;
	public GameObject AudioS;
	public AudioClip FxBallon;
	private int playerTouchBall;

	// Use this for initialization
	void Start()
	{
		playerTouchBall = 0;
	}

	void OnTriggerEnter2D(Collider2D obj)
	{
		if(obj.gameObject.name.Contains("PlayerGod"))
		{
			//Fx
			AudioS.GetComponent<AudioSource>().PlayOneShot(FxBallon);
			//print("La balle a touché le joueur " + obj.GetComponent<PlayerControllerMartin>().idJoueur);
			playerTouchBall = obj.GetComponent<PlayerControllerMartin>().idJoueur;
			//Couleur du barde
			if(playerTouchBall == 1) gameObject.transform.FindChild("Couleur").GetComponent<SpriteRenderer>().color = Color.red;
			else if(playerTouchBall == 2) gameObject.transform.FindChild("Couleur").GetComponent<SpriteRenderer>().color = Color.blue;
			else if(playerTouchBall == 3) gameObject.transform.FindChild("Couleur").GetComponent<SpriteRenderer>().color = new Color(0,0.8f,0.2f);
			else if(playerTouchBall == 4) gameObject.transform.FindChild("Couleur").GetComponent<SpriteRenderer>().color = new Color(0.8f,0,0.9f);
		}
		if(obj.gameObject.name == "But" || obj.gameObject.name == "But2")
		{
			if(playerTouchBall > 0)
			{
				//print("But du joueur " + playerTouchBall + " !");
				GM.GetComponent<GameManagerBardeBall>().GestionBut(playerTouchBall, 1, obj.gameObject);
				playerTouchBall = 0;
				gameObject.transform.FindChild("Couleur").GetComponent<SpriteRenderer>().color = Color.white;
			}
		}
		if(obj.gameObject.name == "ButMini")
		{
			if(playerTouchBall > 0)
			{
				//print("But du joueur " + playerTouchBall + " !");
				GM.GetComponent<GameManagerBardeBall>().GestionBut(playerTouchBall, 2, obj.gameObject);
				playerTouchBall = 0;
				gameObject.transform.FindChild("Couleur").GetComponent<SpriteRenderer>().color = Color.white;
			}
		}
	}
}
